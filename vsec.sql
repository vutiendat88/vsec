/*
Navicat MySQL Data Transfer

Source Server         : Localhost
Source Server Version : 50505
Source Host           : localhost:3306
Source Database       : vsec

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2017-09-04 23:12:09
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for migration
-- ----------------------------
DROP TABLE IF EXISTS `migration`;
CREATE TABLE `migration` (
  `version` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `apply_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of migration
-- ----------------------------
INSERT INTO `migration` VALUES ('m000000_000000_base', '1504432690');
INSERT INTO `migration` VALUES ('m130524_201442_init', '1504432692');

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password_reset_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bcid` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fullname` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` smallint(6) NOT NULL DEFAULT '10',
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `password_reset_token` (`password_reset_token`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('1', 'demo', '7oC19x8jQUb22NNOLHn_9KbAfhYlayVG', '$2y$13$vCKoY7BIHbD/fJNpOznBeuLfeOLDsk8zbD/gDQw5gdR/UN1NANq86', null, 'bws/11438/1', 'DEMO TIEN DAT', 'demo@gmail.com', '10', '1504432714', '1504517686');
INSERT INTO `user` VALUES ('2', 'demo2', 've-jFuOlqFmKdU3T3y6ewFwiXoKtalff', '$2y$13$vCKoY7BIHbD/fJNpOznBeuLfeOLDsk8zbD/gDQw5gdR/UN1NANq86', null, null, 'Demo Tien Dat', 'demo2@gmail.com', '10', '1504433516', '1504433516');
INSERT INTO `user` VALUES ('3', 'demo3', 'xyhzwNBaVMFSvI4xpjkp62oXbDroyGsx', '$2y$13$pFzbmpqCjeaWGXT89bCEK.OipI8w6vPnFEYS3jYyt4zU8s.mcHteq', null, null, 'Vu Tien Dat', 'a@gmail.com', '10', '1504516019', '1504516019');
