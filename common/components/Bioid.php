<?php
/**
 * Created by PhpStorm.
 * User: vutie
 * Date: 9/3/2017
 * Time: 3:17 PM
 */

namespace common\components;


use yii\base\Object;
use yii\helpers\ArrayHelper;

class Bioid extends Object
{
    public $url = 'https://bws.bioid.com/extension';
    private $appId = '852628210.11438.app.bioid.com';
    private $appSecret = 'rZhGdvL1dob0Jnq2CtxKsYoX';

    public $storage = 'bws';
    public $partition = '11438';
    public $classId = 0;

    /**
     * @param string $task
     * @param int $cId
     * @return mixed
     */
    public function getToken($params = [])
    {
        $curl = new Curl();
        $curl->setOption(CURLOPT_USERPWD, $this->appId . ':' . $this->appSecret);
        $getParams = ArrayHelper::merge([
            'id' => $this->appId,
            'bcid'=> $this->storage . '.' . $this->partition . '.' . $this->classId,
            'task' => 'enroll'
        ], $params);
        $curl->setGetParams($getParams);
        $response = $curl->get($this->url . '/token');
        return $response;
    }

    public function upload()
    {

    }

    public function identify()
    {

    }

    /**
     * @param $classId
     */
    public function deleteClass($classId)
    {
        $curl = new Curl();
        $curl->setOption(CURLOPT_USERPWD, $this->appId . ':' . $this->appSecret);
        $curl->setGetParams([
            'bcid'=> $this->storage . '.' . $this->partition . '.' . $classId,
        ]);
        return $curl->delete($this->url . '/deleteclass');
    }

    /**
     * @param $token
     * @return mixed
     */
    public function result($token)
    {
        $curl = new Curl();
        $curl->setOption(CURLOPT_USERPWD, $this->appId . ':' . $this->appSecret);
        $curl->setGetParams([
            'access_token' => $token
        ]);
        $response = $curl->get($this->url . '/result');
        return $response;
    }
}