function __log(e, data) {
    log.innerHTML = "\n" + e + " " + (data || '');
}

var audio_context;
var recorder;

function startUserMedia(stream) {
    var input = audio_context.createMediaStreamSource(stream);
    __log('Media stream created.');

    // Uncomment if you want the audio to feedback directly
    //input.connect(audio_context.destination);
    //__log('Input connected to audio context destination.');

    recorder = new Recorder(input);
    __log('Recorder initialised.');

    inputPoint = audio_context.createGain();
    analyserNode = audio_context.createAnalyser();
    analyserNode.fftSize = 2048;
    input.connect( analyserNode );
    updateAnalysers();
}

function startRecording(button) {
    recorder && recorder.record();
    button.disabled = true;
    button.nextElementSibling.disabled = false;
    __log('Recording...');
}

function stopRecording(button) {
    recorder && recorder.stop();
    button.disabled = true;
    button.previousElementSibling.disabled = false;
    __log('Stopped recording.');

    // create WAV download link using audio data blob
    createDownloadLink();

    recorder.clear();
}

function createDownloadLink() {
    recorder && recorder.exportWAV(function(blob) {
        var url = URL.createObjectURL(blob);
        var li = document.createElement('li');
        var au = document.createElement('audio');
        var hf = document.createElement('a');
        var dl = document.createElement('img');

        au.controls = true;
        au.src = url;
        dl.src = '/img/download.png';
        hf.href = url;
        hf.download = new Date().toISOString() + '.wav';
        hf.appendChild(dl);
        li.appendChild(au);
        li.appendChild(hf);
        recordingslist.appendChild(li);

        upload(blob);
    });
}

function upload(blob) {
    var reader = new window.FileReader();
    reader.readAsDataURL(blob);
    reader.onloadend = function() {
        base64data = reader.result;

        var jqxhr = $.ajax({
            type: "POST",
            url: "https://bws.bioid.com/extension/upload?trait=voice",
            data: base64data,
            headers: { "Authorization": "Bearer " + token } // authentication header
        }).done(function (data, textStatus, jqXHR) {
            if (data.Accepted) {
                console.log('upload succeeded', data.Warnings);
                __log('Upload succeeded');
            } else {
                console.log('upload error', data.Error);
                __log('Upload error' + data.Error);
            }
        }).fail(function (jqXHR, textStatus, errorThrown) {
            console.log('upload failed', textStatus, errorThrown, jqXHR.responseText);
        });
    }
}

function enrollVoice(button) {
    button.disabled = true;
    var jqxhr = $.ajax({
        type: "GET",
        url: "https://bws.bioid.com/extension/enroll",
        headers: { "Authorization": "Bearer " + token }
    }).done(function (data, textStatus, jqXHR) {
        if (data.Success) {
            console.log('enrollment succeeded');
            __log('Enrollment succeeded');
        } else {
            console.log('enrollment failed ', data.Error);
            __log('Enrollment failed ' + data.Error);
        }
    });
}

function checkVoice(button) {
    button.disabled = true;
    alert('Chuc nang dang phat trien');
}
window.onload = function init() {
    try {
        // webkit shim
        window.AudioContext = window.AudioContext || window.webkitAudioContext;
        navigator.getUserMedia = navigator.getUserMedia || navigator.webkitGetUserMedia;
        window.URL = window.URL || window.webkitURL;

        audio_context = new AudioContext;

        __log('Audio context set up.');
        __log('navigator.getUserMedia ' + (navigator.getUserMedia ? 'available.' : 'not present!'));
    } catch (e) {
        alert('No web audio support in this browser!');
    }

    navigator.getUserMedia({audio: true}, startUserMedia, function(e) {
        __log('No live audio input: ' + e);
    });
};



analyserContext = null;

function cancelAnalyserUpdates() {
    window.cancelAnimationFrame( rafID );
    rafID = null;
}
function updateAnalysers(time) {
    if (!analyserContext) {
        var canvas = document.getElementById("analyser");
        canvasWidth = canvas.width;
        canvasHeight = canvas.height;
        analyserContext = canvas.getContext('2d');
    }

    // analyzer draw code here
    {
        var SPACING = 3;
        var BAR_WIDTH = 1;
        var numBars = Math.round(canvasWidth / SPACING);
        var freqByteData = new Uint8Array(analyserNode.frequencyBinCount);

        analyserNode.getByteFrequencyData(freqByteData);

        analyserContext.clearRect(0, 0, canvasWidth, canvasHeight);
        analyserContext.fillStyle = '#F6D565';
        analyserContext.lineCap = 'round';
        var multiplier = analyserNode.frequencyBinCount / numBars;

        // Draw rectangle for each frequency bin.
        for (var i = 0; i < numBars; ++i) {
            var magnitude = 0;
            var offset = Math.floor( i * multiplier );
            // gotta sum/average the block, or we miss narrow-bandwidth spikes
            for (var j = 0; j< multiplier; j++)
                magnitude += freqByteData[offset + j];
            magnitude = magnitude / multiplier;
            var magnitude2 = freqByteData[i * multiplier];
            analyserContext.fillStyle = "hsl( " + Math.round((i*360)/numBars) + ", 100%, 50%)";
            analyserContext.fillRect(i * SPACING, canvasHeight, BAR_WIDTH, -magnitude);
        }
    }

    rafID = window.requestAnimationFrame( updateAnalysers );
}