var capture = null;
var executions = 3;

/*
var token = "the token needs to be requested and filled in here by your web server";
*/
$(document).ready(function () {
    if(typeof task !== 'undefined'){
        capture = bws.initcapture(
            document.getElementById('bwscanvas'),
            document.getElementById('bwsmotionbar'),
            token,
            {
                task: task
            });
        onStart();
    }
});

function onStart() {
    capture.start(function () {
        // bws capture is running - enable mirror and of course start
        $("#bwsmirror").show().click(capture.mirror);
        $("#bwsstart").show().click(function () {
            $("#bwsstart").attr('disabled','disabled');
            capture.startCountdown(function () {
                capture.startRecording();
            });
        });
    }, function (error) {
        // an error occurred during startup
        if (error !== undefined) {
            $("#bwserror").show().html("Video capture failed to start with error: " + error);
        } else {
            // no error info typically says that browser doesn't support getUserMedia
            $("#bwserror").show().html("Your browser does not support the HTML5 Media Capture and Streams API.");
        }
    }, function (error, retry) {
        // done, maybe with error
        capture.stopRecording();
        executions--;
        if (error !== undefined && retry && executions > 0) {
            // if failed, restart if retries are left, but wait a bit until the user has read the error message!
            setTimeout(function () {
                capture.startCountdown(function () {
                    capture.startRecording();
                });
            }, 1800);
        } else {
            // done: redirect to caller -> url needs to be provided by your server
            // Note that the server typically uses the provided token and the
            // BWS Result Extension to fetch the result generated here.
            var url = "/bioid/callback.html?access_token=" + token;
            if (error !== undefined) {
                url = url + "&error=" + error;
            }
            window.location.replace(url);
        }
    }, function (status, message, dataURL) {
        // the function used to inform us about the current bws capture status
        if (status !== 'DisplayTag') { // challenge-response messages are not supported in this sample
            // just display the status code on the screen
            $("#bwsmessage").html(status);
        }
        // some status codes you might be interested in
        if (status === 'Uploading') {
            // begin an upload
        } else if (status === 'Uploaded') {
            // successful upload (we should have a dataURL)
        } else if (status === 'NoFaceFound' || status === 'MultipleFacesFound') {
            // upload failed
        }
    });
};



