<?php

/* @var $this yii\web\View */

use \yii\helpers\Url;

$this->title = 'My Yii Application';

?>
<div class="site-index">

    <div class="body-content">

        <div class="row">
            <div class="col-lg-6">
                <h2>Nhận diện khuôn mặt</h2>

                <ul class="info-service">
                    <li>Yêu cầu được cấp quyền cho website được sử dụng camera.</li>
                    <li>Trình duyệt hỗ trợ Html5: Chrome, Firefox, Microsoft Edge...</li>
                    <li>Website có sử dụng chứng chỉ bảo mật SSL</li>
                </ul>

                <p><a class="btn btn-default" href="<?php echo Url::toRoute(['bioid/register']);?>">Đăng ký khuôn mặt &raquo;</a></p>
                <p><a class="btn btn-default" href="<?php echo Url::toRoute(['bioid/checker']);?>">Nhận diện khuôn mặt &raquo;</a></p>
            </div>
            <div class="col-lg-6">
                <h2>Nhận diện giọng nói</h2>

                <p>Please note that we are currently not capable of recording voice data with the BioID unified user interface or the BioID Apps!
                    We are working on this issue. You can test audio using Silverlight (e.g. with an Internet Explorer) </p>

                <p><a class="btn btn-default" href="<?php echo Url::toRoute(['bioid/register-voice']);?>">Đăng ký giọng nói &raquo;</a></p>
                <p><a class="btn btn-default" href="<?php echo Url::toRoute(['bioid/checker-voice']);?>">Nhận diện giọng nói &raquo;</a></p>
            </div>
        </div>

    </div>
</div>
