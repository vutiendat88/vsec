<?php
/**
 * Created by PhpStorm.
 * User: vutie
 * Date: 9/3/2017
 * Time: 11:57 AM
 */

?>
<div class="main">

    <div class="bwsface">
        <h1 class="heading">Nhận diện giọng nói</h1>
        <canvas id="analyser" width="300" height="100"></canvas>

        <ul id="recordingslist"></ul>
        <div class="bwsnotify" id="log"></div>
        <div class="bwscontrol">
            <button class="btn btn-warning" onclick="startRecording(this);">Record</button>
            <button class="btn btn-danger" onclick="stopRecording(this);" disabled>Stop</button>
            <button class="btn btn-primary" onclick="checkVoice(this)">Check</button>
        </div>

    </div>
</div>
