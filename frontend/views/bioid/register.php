<?php
/**
 * Created by PhpStorm.
 * User: vutie
 * Date: 9/3/2017
 * Time: 11:57 AM
 */

?>
<div class="main">

    <div class="bwsface">
        <h1 class="heading">Đăng ký khuôn mặt</h1>
        <canvas id="bwscanvas"></canvas>
        <canvas id="bwsmotionbar"></canvas>

        <div class="bwsnotify">
            <div id="bwserror" style="display:none"></div>
            <div id="bwsmessage">Camera initialised.</div>
        </div>
        <div class="bwscontrol row">
            <div class="col-xs-6">
                <button id="bwsmirror" class="btn btn-info" style="display:none">Mirror</button>
            </div>
            <div class="col-xs-6">
                <button id="bwsstart" class="btn btn-primary" style="display:none">Start</button>
            </div>
        </div>

    </div>
</div>
