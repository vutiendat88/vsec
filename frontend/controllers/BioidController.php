<?php
/**
 * Created by PhpStorm.
 * User: vutie
 * Date: 9/3/2017
 * Time: 11:59 AM
 */

namespace frontend\controllers;


use common\components\Bioid;
use frontend\models\User;
use Yii;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\View;

class BioidController extends Controller
{

    public function beforeAction($action)
    {
        if (parent::beforeAction($action)) {
            if(Yii::$app->user->isGuest){
                Yii::$app->session->setFlash('error', 'Bạn cần phải đăng nhập để sử dụng chức năng này.');
                $this->redirect(Url::toRoute(['site/login']));
                Yii::$app->end();
            }
            return true;
        }

        return false;

    }

    public function actionRegister()
    {
        $bioid = new Bioid(['classId' => Yii::$app->user->id]);
        $token = $bioid->getToken();
        $script = <<< JS
                var token = '$token';
                var task = 'enrollment';
JS;
        $this->getView()->registerJs($script, View::POS_HEAD);

        $this->view->registerJsFile(
            '@web/js/bws.capture.js',
            ['depends' => [\yii\web\JqueryAsset::className()]]
        );
        $this->view->registerJsFile(
            '@web/js/vsec.js',
            ['depends' => [\yii\web\JqueryAsset::className()]]
        );

        return $this->render('register');
    }

    public function actionChecker()
    {
        $bioid = new Bioid(['classId' => Yii::$app->user->id]);
        $token = $bioid->getToken(['task'=>'verify']);
        $script = <<< JS
            var token = '$token';
            var task = 'verification';
JS;
        $this->getView()->registerJs($script, View::POS_HEAD);

        $this->view->registerJsFile(
            '@web/js/bws.capture.js',
            ['depends' => [\yii\web\JqueryAsset::className()]]
        );
        $this->view->registerJsFile(
            '@web/js/vsec.js',
            ['depends' => [\yii\web\JqueryAsset::className()]]
        );

        return $this->render('checker');
    }

    /**
     * @return string
     */
    public function actionRegisterVoice()
    {
        $bioid = new Bioid(['classId' => Yii::$app->user->id]);
        $token = $bioid->getToken(['livedetection'=>false]);
        $script = <<< JS
            var token = '$token';
JS;
        $this->getView()->registerJs($script, View::POS_HEAD);

        $this->view->registerJsFile(
            '@web/js/recorder.js',
            ['depends' => [\yii\web\JqueryAsset::className()]]
        );
        $this->view->registerJsFile(
            '@web/js/recorderVsec.js',
            ['depends' => [\yii\web\JqueryAsset::className()]]
        );

        return $this->render('register-voice');
    }

    public function actionCheckerVoice()
    {
        $bioid = new Bioid(['classId' => Yii::$app->user->id]);
        $token = $bioid->getToken(['task' => 'verify']);
        $script = <<< JS
            var token = '$token';
JS;
        $this->getView()->registerJs($script, View::POS_HEAD);

        $this->view->registerJsFile(
            '@web/js/recorder.js',
            ['depends' => [\yii\web\JqueryAsset::className()]]
        );
        $this->view->registerJsFile(
            '@web/js/recorderVsec.js',
            ['depends' => [\yii\web\JqueryAsset::className()]]
        );

        return $this->render('checker-voice');
    }

    /**
     * Service not configured to handle this request
     */
    public function actionIdentify()
    {
        return $this->render('identify');
    }

    public function actionCallback()
    {
        $bioid = new Bioid();
        $result = $bioid->result(Yii::$app->request->get('access_token'));

        if (empty($result) || !$result) {
            $this->redirect(Url::home());
            Yii::$app->end();
        }
        $callback = Json::decode($result);
        if(isset($callback['Success']) && $callback['Success']) {
            if ($callback['Action'] === 'enrollment') {
                $user = Yii::$app->user->identity;
                $user->bcid = $callback['BCID'];
                $user->save(false);
                Yii::$app->session->setFlash('success', 'Registration success' . ' ' . Json::encode($callback));
            } else {
                $user = User::findOne(['bcid' => $callback['BCID']]);
                $fullname = $user->fullname;
                Yii::$app->session->setFlash('success', 'Hello: ' . $fullname . ' ' . Json::encode($callback));
            }
        } else {
            Yii::$app->session->setFlash('error', 'Không nhận dạng được ' . Json::encode($callback));
        }

        $this->redirect(Url::home());
        Yii::$app->end();
    }
}